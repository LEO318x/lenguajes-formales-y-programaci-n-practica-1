﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MenuStrip_MenuPrincipal = New System.Windows.Forms.MenuStrip()
        Me.ArchivoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NuevaPestañaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CargarArchivoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GuardarArchivoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SalirToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AyudaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ManualAplicaciónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AcercaDeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TabControl_Pestañas = New System.Windows.Forms.TabControl()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btn_stop = New System.Windows.Forms.Button()
        Me.btn_play = New System.Windows.Forms.Button()
        Me.btn_html = New System.Windows.Forms.Button()
        Me.btn_scanner = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.ListView_Tokens = New System.Windows.Forms.ListView()
        Me.Numero = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Lexema = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ID_Token = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Token = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ListView_Notas = New System.Windows.Forms.ListView()
        Me.No = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Nota = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Label_Nota = New System.Windows.Forms.Label()
        Me.ListView_Errores = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label_Cancion = New System.Windows.Forms.Label()
        Me.MenuStrip_MenuPrincipal.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip_MenuPrincipal
        '
        Me.MenuStrip_MenuPrincipal.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ArchivoToolStripMenuItem, Me.AyudaToolStripMenuItem})
        Me.MenuStrip_MenuPrincipal.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip_MenuPrincipal.Name = "MenuStrip_MenuPrincipal"
        Me.MenuStrip_MenuPrincipal.Size = New System.Drawing.Size(674, 24)
        Me.MenuStrip_MenuPrincipal.TabIndex = 0
        Me.MenuStrip_MenuPrincipal.Text = "MenuStrip_MenuPrincipal"
        '
        'ArchivoToolStripMenuItem
        '
        Me.ArchivoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NuevaPestañaToolStripMenuItem, Me.CargarArchivoToolStripMenuItem, Me.GuardarArchivoToolStripMenuItem, Me.SalirToolStripMenuItem})
        Me.ArchivoToolStripMenuItem.Name = "ArchivoToolStripMenuItem"
        Me.ArchivoToolStripMenuItem.Size = New System.Drawing.Size(60, 20)
        Me.ArchivoToolStripMenuItem.Text = "&Archivo"
        '
        'NuevaPestañaToolStripMenuItem
        '
        Me.NuevaPestañaToolStripMenuItem.Name = "NuevaPestañaToolStripMenuItem"
        Me.NuevaPestañaToolStripMenuItem.Size = New System.Drawing.Size(160, 22)
        Me.NuevaPestañaToolStripMenuItem.Text = "&Nueva Pestaña"
        '
        'CargarArchivoToolStripMenuItem
        '
        Me.CargarArchivoToolStripMenuItem.Name = "CargarArchivoToolStripMenuItem"
        Me.CargarArchivoToolStripMenuItem.Size = New System.Drawing.Size(160, 22)
        Me.CargarArchivoToolStripMenuItem.Text = "&Cargar Archivo"
        '
        'GuardarArchivoToolStripMenuItem
        '
        Me.GuardarArchivoToolStripMenuItem.Name = "GuardarArchivoToolStripMenuItem"
        Me.GuardarArchivoToolStripMenuItem.Size = New System.Drawing.Size(160, 22)
        Me.GuardarArchivoToolStripMenuItem.Text = "&Guardar Archivo"
        '
        'SalirToolStripMenuItem
        '
        Me.SalirToolStripMenuItem.Name = "SalirToolStripMenuItem"
        Me.SalirToolStripMenuItem.Size = New System.Drawing.Size(160, 22)
        Me.SalirToolStripMenuItem.Text = "&Salir"
        '
        'AyudaToolStripMenuItem
        '
        Me.AyudaToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ManualAplicaciónToolStripMenuItem, Me.AcercaDeToolStripMenuItem})
        Me.AyudaToolStripMenuItem.Name = "AyudaToolStripMenuItem"
        Me.AyudaToolStripMenuItem.Size = New System.Drawing.Size(53, 20)
        Me.AyudaToolStripMenuItem.Text = "&Ayuda"
        '
        'ManualAplicaciónToolStripMenuItem
        '
        Me.ManualAplicaciónToolStripMenuItem.Name = "ManualAplicaciónToolStripMenuItem"
        Me.ManualAplicaciónToolStripMenuItem.Size = New System.Drawing.Size(173, 22)
        Me.ManualAplicaciónToolStripMenuItem.Text = "&Manual Aplicación"
        '
        'AcercaDeToolStripMenuItem
        '
        Me.AcercaDeToolStripMenuItem.Name = "AcercaDeToolStripMenuItem"
        Me.AcercaDeToolStripMenuItem.Size = New System.Drawing.Size(173, 22)
        Me.AcercaDeToolStripMenuItem.Text = "&Acerca de..."
        '
        'TabControl_Pestañas
        '
        Me.TabControl_Pestañas.Location = New System.Drawing.Point(12, 27)
        Me.TabControl_Pestañas.Name = "TabControl_Pestañas"
        Me.TabControl_Pestañas.SelectedIndex = 0
        Me.TabControl_Pestañas.Size = New System.Drawing.Size(374, 413)
        Me.TabControl_Pestañas.TabIndex = 1
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(434, 27)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(100, 23)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "Crear Pestaña"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(540, 27)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(89, 23)
        Me.Button2.TabIndex = 3
        Me.Button2.Text = "Cerrar Pestaña"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btn_stop)
        Me.GroupBox1.Controls.Add(Me.btn_play)
        Me.GroupBox1.Controls.Add(Me.btn_html)
        Me.GroupBox1.Controls.Add(Me.btn_scanner)
        Me.GroupBox1.Location = New System.Drawing.Point(433, 81)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(195, 102)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Funciones"
        '
        'btn_stop
        '
        Me.btn_stop.Location = New System.Drawing.Point(94, 60)
        Me.btn_stop.Name = "btn_stop"
        Me.btn_stop.Size = New System.Drawing.Size(75, 23)
        Me.btn_stop.TabIndex = 3
        Me.btn_stop.Text = "Stop"
        Me.btn_stop.UseVisualStyleBackColor = True
        '
        'btn_play
        '
        Me.btn_play.Location = New System.Drawing.Point(7, 61)
        Me.btn_play.Name = "btn_play"
        Me.btn_play.Size = New System.Drawing.Size(75, 23)
        Me.btn_play.TabIndex = 2
        Me.btn_play.Text = "Play"
        Me.btn_play.UseVisualStyleBackColor = True
        '
        'btn_html
        '
        Me.btn_html.Location = New System.Drawing.Point(94, 19)
        Me.btn_html.Name = "btn_html"
        Me.btn_html.Size = New System.Drawing.Size(75, 23)
        Me.btn_html.TabIndex = 1
        Me.btn_html.Text = "Archivos"
        Me.btn_html.UseVisualStyleBackColor = True
        '
        'btn_scanner
        '
        Me.btn_scanner.Location = New System.Drawing.Point(7, 20)
        Me.btn_scanner.Name = "btn_scanner"
        Me.btn_scanner.Size = New System.Drawing.Size(75, 23)
        Me.btn_scanner.TabIndex = 0
        Me.btn_scanner.Text = "Scanner"
        Me.btn_scanner.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(647, 101)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 23)
        Me.Button3.TabIndex = 5
        Me.Button3.Text = "Test Hilo"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(647, 130)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 23)
        Me.Button4.TabIndex = 6
        Me.Button4.Text = "Scanner"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'ListView_Tokens
        '
        Me.ListView_Tokens.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.Numero, Me.Lexema, Me.ID_Token, Me.Token})
        Me.ListView_Tokens.Location = New System.Drawing.Point(223, 446)
        Me.ListView_Tokens.Name = "ListView_Tokens"
        Me.ListView_Tokens.Size = New System.Drawing.Size(87, 97)
        Me.ListView_Tokens.TabIndex = 7
        Me.ListView_Tokens.UseCompatibleStateImageBehavior = False
        Me.ListView_Tokens.View = System.Windows.Forms.View.Details
        '
        'Numero
        '
        Me.Numero.Text = "#"
        Me.Numero.Width = 35
        '
        'Lexema
        '
        Me.Lexema.Text = "Lexema"
        Me.Lexema.Width = 119
        '
        'ID_Token
        '
        Me.ID_Token.Text = "ID Token"
        Me.ID_Token.Width = 123
        '
        'Token
        '
        Me.Token.Text = "Token"
        Me.Token.Width = 143
        '
        'ListView_Notas
        '
        Me.ListView_Notas.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.No, Me.Nota})
        Me.ListView_Notas.Location = New System.Drawing.Point(116, 446)
        Me.ListView_Notas.Name = "ListView_Notas"
        Me.ListView_Notas.Size = New System.Drawing.Size(98, 97)
        Me.ListView_Notas.TabIndex = 8
        Me.ListView_Notas.UseCompatibleStateImageBehavior = False
        Me.ListView_Notas.View = System.Windows.Forms.View.Details
        '
        'No
        '
        Me.No.Text = "#"
        '
        'Nota
        '
        Me.Nota.Text = "Nota"
        '
        'Label_Nota
        '
        Me.Label_Nota.AutoSize = True
        Me.Label_Nota.Font = New System.Drawing.Font("Lucida Sans", 48.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_Nota.Location = New System.Drawing.Point(74, 64)
        Me.Label_Nota.Name = "Label_Nota"
        Me.Label_Nota.Size = New System.Drawing.Size(51, 72)
        Me.Label_Nota.TabIndex = 9
        Me.Label_Nota.Text = "-"
        '
        'ListView_Errores
        '
        Me.ListView_Errores.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader3, Me.ColumnHeader4, Me.ColumnHeader5})
        Me.ListView_Errores.Location = New System.Drawing.Point(12, 446)
        Me.ListView_Errores.Name = "ListView_Errores"
        Me.ListView_Errores.Size = New System.Drawing.Size(98, 97)
        Me.ListView_Errores.TabIndex = 10
        Me.ListView_Errores.UseCompatibleStateImageBehavior = False
        Me.ListView_Errores.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "#"
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Fila"
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Columna"
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Lexema"
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Descripción"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label_Cancion)
        Me.GroupBox2.Controls.Add(Me.Label_Nota)
        Me.GroupBox2.Location = New System.Drawing.Point(426, 214)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(207, 170)
        Me.GroupBox2.TabIndex = 11
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Cancion"
        '
        'Label_Cancion
        '
        Me.Label_Cancion.AutoSize = True
        Me.Label_Cancion.Location = New System.Drawing.Point(6, 21)
        Me.Label_Cancion.Name = "Label_Cancion"
        Me.Label_Cancion.Size = New System.Drawing.Size(10, 13)
        Me.Label_Cancion.TabIndex = 10
        Me.Label_Cancion.Text = "-"
        Me.Label_Cancion.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(674, 492)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.ListView_Errores)
        Me.Controls.Add(Me.ListView_Notas)
        Me.Controls.Add(Me.ListView_Tokens)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.TabControl_Pestañas)
        Me.Controls.Add(Me.MenuStrip_MenuPrincipal)
        Me.MainMenuStrip = Me.MenuStrip_MenuPrincipal
        Me.Name = "Form1"
        Me.Text = "[LFP_1S2016] - Practica #1"
        Me.MenuStrip_MenuPrincipal.ResumeLayout(False)
        Me.MenuStrip_MenuPrincipal.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MenuStrip_MenuPrincipal As MenuStrip
    Friend WithEvents ArchivoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents NuevaPestañaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CargarArchivoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents GuardarArchivoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SalirToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AyudaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ManualAplicaciónToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AcercaDeToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TabControl_Pestañas As TabControl
    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents btn_stop As Button
    Friend WithEvents btn_play As Button
    Friend WithEvents btn_html As Button
    Friend WithEvents btn_scanner As Button
    Friend WithEvents Button3 As Button
    Friend WithEvents Button4 As Button
    Friend WithEvents ListView_Tokens As ListView
    Friend WithEvents Numero As ColumnHeader
    Friend WithEvents Lexema As ColumnHeader
    Friend WithEvents ID_Token As ColumnHeader
    Friend WithEvents Token As ColumnHeader
    Friend WithEvents ListView_Notas As ListView
    Friend WithEvents No As ColumnHeader
    Friend WithEvents Nota As ColumnHeader
    Friend WithEvents Label_Nota As Label
    Friend WithEvents ListView_Errores As ListView
    Friend WithEvents ColumnHeader1 As ColumnHeader
    Friend WithEvents ColumnHeader2 As ColumnHeader
    Friend WithEvents ColumnHeader3 As ColumnHeader
    Friend WithEvents ColumnHeader4 As ColumnHeader
    Friend WithEvents ColumnHeader5 As ColumnHeader
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents Label_Cancion As Label
End Class
