﻿'Este proyecto fue creado por Mike Molina, estudiante de la Universidad de San Carlos de Guatemala.
'Repositorio Git https://bitbucket.org/LEO318x/lenguajes-formales-y-programaci-n-practica-1
'Visita mi página Web: http://leo318x.ml


Public Class AnalizadorLexico
    Dim contadorCaracter As Integer = 0
    Dim contadorColumnas As Integer = 1
    Dim contadorErrores As Integer = 1
    Dim cadena As String
    Dim palabraTemporal As String = ""
    Dim caracter() As Char
    Dim filaContador As Integer
    Dim cancion = False, nombreCancion = False, nota = False
    Dim listaTokens As New Dictionary(Of String, String)
    Public Sub New(fila As Integer, texto As String)
        listaTokens.Add("<CANCION>", "1")
        listaTokens.Add("<NOMBRE>", "2")
        listaTokens.Add("<PARTITURA>", "3")
        listaTokens.Add("<NOTA>", "4")
        listaTokens.Add("</CANCION>", "5")
        listaTokens.Add("</PARTITURA>", "6")
        listaTokens.Add("</NOMBRE>", "7")
        listaTokens.Add("</NOTA>", "8")
        listaTokens.Add("A0", "10")
        listaTokens.Add("A1", "11")
        listaTokens.Add("A2", "12")
        listaTokens.Add("A3", "13")
        listaTokens.Add("A4", "14")
        listaTokens.Add("A5", "15")
        listaTokens.Add("A6", "16")
        listaTokens.Add("A7", "17")
        listaTokens.Add("A8", "18")
        listaTokens.Add("A9", "19")

        listaTokens.Add("B0", "20")
        listaTokens.Add("B1", "21")
        listaTokens.Add("B2", "22")
        listaTokens.Add("B3", "23")
        listaTokens.Add("B4", "24")
        listaTokens.Add("B5", "25")
        listaTokens.Add("B6", "26")
        listaTokens.Add("B7", "27")
        listaTokens.Add("B8", "28")
        listaTokens.Add("B9", "29")

        listaTokens.Add("C0", "30")
        listaTokens.Add("C1", "31")
        listaTokens.Add("C2", "32")
        listaTokens.Add("C3", "33")
        listaTokens.Add("C4", "34")
        listaTokens.Add("C5", "35")
        listaTokens.Add("C6", "36")
        listaTokens.Add("C7", "37")
        listaTokens.Add("C8", "38")
        listaTokens.Add("C9", "39")

        listaTokens.Add("D0", "40")
        listaTokens.Add("D1", "41")
        listaTokens.Add("D2", "42")
        listaTokens.Add("D3", "43")
        listaTokens.Add("D4", "44")
        listaTokens.Add("D5", "45")
        listaTokens.Add("D6", "46")
        listaTokens.Add("D7", "47")
        listaTokens.Add("D8", "48")
        listaTokens.Add("D9", "49")

        listaTokens.Add("E0", "50")
        listaTokens.Add("E1", "51")
        listaTokens.Add("E2", "52")
        listaTokens.Add("E3", "53")
        listaTokens.Add("E4", "54")
        listaTokens.Add("E5", "55")
        listaTokens.Add("E6", "56")
        listaTokens.Add("E7", "57")
        listaTokens.Add("E8", "58")
        listaTokens.Add("E9", "59")

        listaTokens.Add("F0", "60")
        listaTokens.Add("F1", "61")
        listaTokens.Add("F2", "62")
        listaTokens.Add("F3", "63")
        listaTokens.Add("F4", "64")
        listaTokens.Add("F5", "65")
        listaTokens.Add("F6", "66")
        listaTokens.Add("F7", "67")
        listaTokens.Add("F8", "68")
        listaTokens.Add("F9", "69")

        listaTokens.Add("G0", "70")
        listaTokens.Add("G1", "71")
        listaTokens.Add("G2", "72")
        listaTokens.Add("G3", "73")
        listaTokens.Add("G4", "74")
        listaTokens.Add("G5", "75")
        listaTokens.Add("G6", "76")
        listaTokens.Add("G7", "77")
        listaTokens.Add("G8", "78")
        listaTokens.Add("G9", "79")
        cadena = texto.ToUpper
        caracter = cadena.ToCharArray
        filaContador = fila
        Inicio()
    End Sub

    Public Sub Inicio()
        'aceptado = False
        contadorCaracter = 0
        x0()
    End Sub



    '  .----.   
    ' /  ..  \  
    '.  /  \  . 
    '|  |  '  | 
    ''  \  /  ' 
    ' \  `'  /  
    '  `---''   
    Public Sub x0()
        If (contadorCaracter < cadena.Length) Then
            Console.WriteLine("x0 - " + palabraTemporal)

            'Si vienen espacios o tabulaciones son ignorados pero el contador de filas y columnas no
            If (Char.IsSeparator(caracter(contadorCaracter)) Or caracter(contadorCaracter) = vbTab) Then
                'If (esError) Then
                '    func_TablaErrores(palabraTemporal, "Desconocido", contadorColumnas, "No Reconocido")
                '    palabraTemporal = ""
                '    esError = False
                'End If
                contadorCaracter += 1
                contadorColumnas += 1
                'Envio a x0
                x0()
            Else
                If (nombreCancion = False) Then
                    If (nota = False) Then
                        If (caracter(contadorCaracter) = "<") Then
                            'If (esError) Then
                            '    func_TablaErrores(palabraTemporal, "Desconocido", contadorColumnas, "No Reconocido")
                            '    palabraTemporal = ""
                            '    esError = False
                            'End If
                            palabraTemporal += caracter(contadorCaracter)
                            contadorCaracter += 1
                            contadorColumnas += 1
                            'Envio a x1
                            x1()
                        Else
                            'Muere en x0
                            Console.WriteLine("Muere en x0")
                            'contadorCaracter += 1
                            'contadorColumnas += 1
                            'x0()
                            xError()
                        End If
                    Else
                        'palabraTemporal += caracter(contadorCaracter)
                        'contadorCaracter += 1
                        'contadorColumnas += 1
                        x4()
                    End If
                Else
                    x7()
                End If


            End If
        End If
    End Sub


    ' .---. 
    '/_   | 
    ' |   | 
    ' |   | 
    ' |   | 
    ' |   | 
    ' `---'
    Public Sub x1()
        If (contadorCaracter < cadena.Length) Then
            Console.WriteLine("x1 - " + palabraTemporal)
            If (Char.IsLetter(caracter(contadorCaracter))) Then
                'If (esError) Then
                '    func_TablaErrores(palabraTemporal, "Desconocido", contadorColumnas, "No Reconocido")
                '    palabraTemporal = ""
                '    esError = False
                'End If
                palabraTemporal += caracter(contadorCaracter)
                contadorCaracter += 1
                contadorColumnas += 1
                'Envio a x2
                x2()
            ElseIf (caracter(contadorCaracter) = "/") Then
                palabraTemporal += caracter(contadorCaracter)
                contadorCaracter += 1
                contadorColumnas += 1
                'Envio a x2
                x2()
            Else
                'Muere en x1
                Console.WriteLine("Muere en x1")
                'palabraTemporal = ""
                'x0()
                xError()
            End If
        End If
    End Sub



    ' .-----.  
    '/ ,-.   \ 
    ''-'  |  | 
    '   .'  /  
    ' .'  /__  
    '|       | 
    '`-------'
    Public Sub x2()
        If (contadorCaracter < cadena.Length) Then
            Console.WriteLine("x2 - " + palabraTemporal)
            If (Char.IsLetter(caracter(contadorCaracter))) Then
                'If (esError) Then
                '    func_TablaErrores(palabraTemporal, "Desconocido", contadorColumnas, "No Reconocido")
                '    palabraTemporal = ""
                '    esError = False
                'End If
                palabraTemporal += caracter(contadorCaracter)
                contadorCaracter += 1
                contadorColumnas += 1
                'Envio a x2
                x2()
            ElseIf (caracter(contadorCaracter) = ">") Then
                palabraTemporal += caracter(contadorCaracter)
                contadorCaracter += 1
                contadorColumnas += 1
                'Envio a x3
                x3()
            Else
                'Muere en x2
                Console.WriteLine("Muere en x1")
                xError()
            End If
        End If
    End Sub

    ' .-----.  
    '/  -.   \ 
    ''-' _'  | 
    '   |_  <  
    '.-.  |  | 
    '\ `-'   / 
    ' `----''  
    Public Sub x3()
        If (TokenObtenido(palabraTemporal)) Then
            If (palabraTemporal = "<NOMBRE>") Then
                nombreCancion = True
            ElseIf (palabraTemporal = "<NOTA>") Then
                nota = True
                'x6()
            End If

            func_TablaReservadas(palabraTemporal, palabraTemporal, listaTokens.Item(palabraTemporal).ToString, 1)
            ' MsgBox("Palabra Aceptada " + palabraTemporal)
            palabraTemporal = ""
            'contadorCaracter += 1
            'contadorColumnas += 1
            x0()
        Else
            'Este error es porque la etiqueta no es valida, regresamos a x0
            'palabraTemporal = ""
            'x0()
            xError()
        End If
    End Sub


    '    .---.   
    '   / .  |   
    '  / /|  |   
    ' / / |  |_  
    '/  '-'    | 
    '`----|  |-' 
    '     `--'
    Public Sub x4()
        If (contadorCaracter < cadena.Length) Then
            Console.WriteLine("x4 --- " + palabraTemporal)
            If (Char.IsLetter(caracter(contadorCaracter))) Then
                palabraTemporal += caracter(contadorCaracter)
                contadorCaracter += 1
                contadorColumnas += 1
                'Envio a x5
                x5()
            Else
                palabraTemporal += caracter(contadorCaracter)
                'contadorCaracter += 1
                'contadorColumnas += 1
                Console.WriteLine("x4error - " + palabraTemporal)
                Console.WriteLine("xerror- " + caracter(contadorCaracter))
                nota = False
                xError()
            End If

        End If
    End Sub


    '.------.  
    '|   ___|  
    '|  '--.   
    '`---.  '. 
    '.-   |  | 
    '| `-'   / 
    ' `----''
    Public Sub x5()
        Console.WriteLine("x5 - " + palabraTemporal)
        'contadorCaracter += 1
        'contadorColumnas += 1
        If (Char.IsNumber(caracter(contadorCaracter))) Then
            palabraTemporal += caracter(contadorCaracter)
            contadorCaracter += 1
            contadorColumnas += 1
            x6()
        Else
            xError()
        End If
    End Sub


    '  ,--.    
    ' /  .'    
    '.  / -.   
    '| .-.  '  
    '' \  |  | 
    '\  `'  /  
    ' `----'   
    Public Sub x6()
        If (contadorCaracter < cadena.Length) Then
            Console.WriteLine("x6 - " + palabraTemporal)

            If (Char.IsLetter(caracter(contadorCaracter)) Or Char.IsNumber(caracter(contadorCaracter))) Then
                xError()
            Else
                If (TokenObtenido(palabraTemporal)) Then
                    'MsgBox("Palabra Aceptada " + palabraTemporal)
                    func_listaNotasMusicales(palabraTemporal)
                    func_TablaReservadas(palabraTemporal, palabraTemporal, listaTokens.Item(palabraTemporal).ToString, 1)

                    palabraTemporal = ""
                    nota = False
                    'contadorCaracter += 1
                    'contadorColumnas += 1
                    x0()
                End If
            End If



            'If (caracter(contadorCaracter) = "<") Then
            '    'If (esError) Then
            '    '    func_TablaErrores(palabraTemporal, "Desconocido", contadorColumnas, "No Reconocido")
            '    '    palabraTemporal = ""
            '    '    esError = False
            '    'End If
            '    Console.WriteLine("x6 - " + palabraTemporal)
            '    palabraTemporal = ""
            '    Console.WriteLine("x6 se va a x0")
            '    nombre = False
            '    'contadorCaracter += 1
            '    'contadorColumnas += 1
            '    'Envio a x5
            '    x0()
            'Else
            '    palabraTemporal += caracter(contadorCaracter)
            '    contadorCaracter += 1
            '    contadorColumnas += 1
            '    x6()
            'End If
        End If
    End Sub


    '.--------. 
    '|   __   ' 
    '`--' .  /  
    '    /  /   
    '   .  /    
    '  /  /     
    ' `--' 
    Public Sub x7()
        'If (contadorCaracter < cadena.Length) Then
        '    Console.WriteLine("x7 - " + palabraTemporal)
        '    If (Char.IsLetter(caracter(contadorCaracter)) Or Char.IsNumber(caracter(contadorCaracter)) Or Char.IsSymbol(caracter(contadorCaracter))) Then
        '        'palabraTemporal += caracter(contadorCaracter)
        '        'contadorCaracter += 1
        '        'contadorColumnas += 1
        '        x8()
        '    Else
        '        'error
        '    End If
        'End If
        If (contadorCaracter < cadena.Length) Then
            Console.WriteLine("x7 - " + palabraTemporal)
            If (caracter(contadorCaracter) = "<") Then
                Console.WriteLine("Nombre canción aceptado" + palabraTemporal)
                func_TablaReservadas(palabraTemporal, "Nombre Cancion", "00", contadorColumnas)
                Form1.nombreCancionlbl = palabraTemporal
                nombreCancion = False
                palabraTemporal = ""
                'nombreCancion = True
                x0()
            Else
                palabraTemporal += caracter(contadorCaracter)
                contadorCaracter += 1
                contadorColumnas += 1
                x7()
            End If

        End If
    End Sub


    '  .-----.   
    ' /  .-.  \  
    '|   \_.' /  
    ' /  .-. '.  
    '|  |   |  | 
    ' \  '-'  /  
    '  `----'' 
    Public Sub x8()
        If (contadorCaracter < cadena.Length) Then
            Console.WriteLine("x8 - " + palabraTemporal)
            If (caracter(contadorCaracter) = "<") Then
                Console.WriteLine("Nombre canción aceptado" + palabraTemporal)
                func_TablaReservadas(palabraTemporal, "Nombre Cancion", "00", contadorColumnas)
                Form1.nombreCancionlbl = palabraTemporal
                nombreCancion = False
                palabraTemporal = ""
                'nombreCancion = True
                x0()
            Else

                palabraTemporal += caracter(contadorCaracter)
                contadorCaracter += 1
                contadorColumnas += 1
                x8()

            End If

        End If
    End Sub

    '      ______                     
    '     |  ____|                    
    '__  _| |__   _ __ _ __ ___  _ __ 
    '\ \/ /  __| | '__| '__/ _ \| '__|
    ' >  <| |____| |  | | | (_) | |   
    '/_/\_\______|_|  |_|  \___/|_|   
    Public Sub xError()
        Console.WriteLine("Entro a error")
        If (contadorCaracter < cadena.Length) Then
            If (caracter(contadorCaracter) = "<") Then
                Console.WriteLine(caracter(contadorCaracter) + "Error - " + palabraTemporal)
                func_TablaErrores(filaContador, contadorColumnas, palabraTemporal, "Desconocido")
                palabraTemporal = ""
                x0()
            Else
                palabraTemporal += caracter(contadorCaracter)
                contadorCaracter += 1
                contadorColumnas += 1
                xError()
            End If
        Else
            func_TablaErrores(filaContador, contadorColumnas, palabraTemporal, "Desconocido")
        End If
    End Sub



    Function TokenObtenido(tmpToken)

        Console.WriteLine(tmpToken)
        If listaTokens.ContainsKey(tmpToken) Then
            Dim valorToken As String = listaTokens.Item(tmpToken)
            Console.WriteLine(valorToken)
            Return True
        Else
            Return False
        End If
    End Function



    Public Function func_TablaReservadas(lexema As String, token As String, idToken As String, posicion As Integer)
        Dim tablaTokens As ListViewItem
        Form1.ListView_Tokens.BeginUpdate()
        tablaTokens = Form1.ListView_Tokens.Items.Add("1")
        tablaTokens.SubItems.Add(lexema)
        If (idToken = "00") Then
            tablaTokens.SubItems.Add(token)
        Else
            If (listaTokens.Item(lexema) > 0 And listaTokens.Item(lexema) < 5) Then
                tablaTokens.SubItems.Add("Reservada apertura " + token)
            ElseIf (listaTokens.Item(lexema) > 4 And listaTokens.Item(lexema) < 9) Then
                tablaTokens.SubItems.Add("Reservada cierre " + token)
            Else
                tablaTokens.SubItems.Add("Nota Musical " + token)
            End If
        End If
        tablaTokens.SubItems.Add(idToken)
        Form1.ListView_Tokens.Update()
        Form1.ListView_Tokens.EndUpdate()
    End Function

    Public Function func_TablaErrores(fila As String, columna As String, lexema As String, tipoError As String)
        Dim tablaErrores As ListViewItem
        Form1.ListView_Errores.BeginUpdate()
        tablaErrores = Form1.ListView_Errores.Items.Add(Form1.contadorErr)
        tablaErrores.SubItems.Add(fila)
        tablaErrores.SubItems.Add(columna - (palabraTemporal.Length))
        tablaErrores.SubItems.Add(lexema)
        tablaErrores.SubItems.Add(tipoError)
        Form1.contadorErr += 1
        contadorErrores += 1
        Form1.ListView_Errores.Update()
        Form1.ListView_Errores.EndUpdate()
    End Function


    Public Function func_listaNotasMusicales(nota As String)
        Dim tablaNotasMusicales As ListViewItem
        Form1.ListView_Notas.BeginUpdate()
        tablaNotasMusicales = Form1.ListView_Notas.Items.Add("#")
        tablaNotasMusicales.SubItems.Add(nota)
        Form1.ListView_Notas.Update()
        Form1.ListView_Notas.EndUpdate()
    End Function
End Class
